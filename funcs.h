/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/

#ifndef _FUNCS_H_
#define _FUNCS_H_

#include <SDL/SDL.h>
#include <lua.hpp>
#include "globals.h"

//function for loading the configuration data
//returns the screen
SDL_Surface *load_config(lua_State *L, std::string pname);

//prints the usage info to STDERR
void print_usage(std::string progname);

//function for clearing out the image store
void clear_images();

//delay function...
static int l_delay(lua_State *L){
  int time = (int)lua_tonumber(L, 1);
  SDL_Delay(time);
  return 0;      
}

static int l_getticks(lua_State *L){
  lua_pushinteger(L, SDL_GetTicks());
  return 1;
}


#endif
