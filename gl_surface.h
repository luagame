/*
Copyright (c)2008 - Brett Lajzer

See LICENSE for license information.
*/

class GLSurface {
public:
	GLSurface();
	GLSurface(GLuint t, unsigned int width, unsigned int height) : texture(t), w(width), h(height){};

	GLuint texture;
	unsigned int w;
	unsigned int h;
};
