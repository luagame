/*
LuaGame Game Execution Environment
----------------------------------

Copyright (c)2006-2008 - Brett Lajzer

See LICENSE for license information.
*/

#include <iostream>
#include <list>
#include <string>
#include <lua.hpp>
#include <SDL/SDL.h>
#include "globals.h"
#include "funcs.h"
#include "funcs_input.h"
#include "funcs_video.h"
#include "funcs_sound.h"
#include "funcs_font.h"
#include "funcs_draw.h"

using namespace std;

int main(int argc, char *argv[]){

    //get path from the command line
	//or print usage info if the arg is --help or -h
	if(argc == 2){
        if(string(argv[1])=="-h" || string(argv[1])=="--help"){
            print_usage(string(argv[0]));
            return 0;
        }else{
            if(chdir(argv[1])==-1){
				perror("");
				return 1;
			}
        }
	}

    //init Lua
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);

    //init SDL
    SDL_Init(SDL_INIT_EVERYTHING);

    //initialize audio
    sound_open();

    //initialize fonts
    TTF_Init();

    //load up configuration file and create screen surface
    screen = load_config(L, string(argv[0]));
    if(!screen){ //die
      cerr << "Error: Cannot create video surface. \n";
      SDL_Quit();
      lua_close(L);
      return 1;
    }

    //register functions with lua    
    lua_pushcfunction(L, l_getimage);
    lua_setglobal(L, "get_image");

    lua_pushcfunction(L, l_releaseimage);
    lua_setglobal(L, "release_image");
    
    lua_pushcfunction(L, l_flip);
    lua_setglobal(L, "update_screen");
    
    lua_pushcfunction(L, l_display);
    lua_setglobal(L, "display");
    
    lua_pushcfunction(L, l_displayframe);
    lua_setglobal(L, "display_frame");
    
    lua_pushcfunction(L, l_mouse_state);
    lua_setglobal(L, "mouse_state");
    
    lua_pushcfunction(L, l_show_cursor);
    lua_setglobal(L, "show_cursor");
    
    lua_pushcfunction(L, l_fill_screen);
    lua_setglobal(L, "fill_screen");
    
    lua_pushcfunction(L, l_delete_image);
    lua_setglobal(L, "delete_image");
    

    //sound functions
    lua_pushcfunction(L, l_playsample);
    lua_setglobal(L, "play_sample");
    
    lua_pushcfunction(L, l_stopsamples);
    lua_setglobal(L, "stop_samples");
    
    lua_pushcfunction(L, l_loadsample);
    lua_setglobal(L, "load_sample");
    
    lua_pushcfunction(L, l_unloadsample);
    lua_setglobal(L, "unload_sample");
    
    lua_pushcfunction(L, l_clearsamples);
    lua_setglobal(L, "clear_samples");

    lua_pushcfunction(L, l_playmusic);
    lua_setglobal(L, "play_music");

    lua_pushcfunction(L, l_stopmusic);
    lua_setglobal(L, "stop_music");

    //font functions
    lua_pushcfunction(L, l_load_font);
    lua_setglobal(L, "load_font");

    lua_pushcfunction(L, l_close_font);
    lua_setglobal(L, "unload_font");

    lua_pushcfunction(L, l_font_string_metrics);
    lua_setglobal(L, "rendered_string_size");

    lua_pushcfunction(L, l_render_string);
    lua_setglobal(L, "render_string");

    //misc functions
    lua_pushcfunction(L, l_get_event);
    lua_setglobal(L, "get_event");

    lua_pushcfunction(L, l_getticks);
    lua_setglobal(L, "get_ticks");

    lua_pushcfunction(L, l_delay);
    lua_setglobal(L, "delay");

    lua_pushcfunction(L, l_num_joysticks);
    lua_setglobal(L, "num_joysticks");

    //draw functions
    lua_pushcfunction(L, l_draw_pixel);
    lua_setglobal(L, "draw_pixel");

    lua_pushcfunction(L, l_draw_line);
    lua_setglobal(L, "draw_line");

    lua_pushcfunction(L, l_draw_rect);
    lua_setglobal(L, "draw_rect");

    lua_pushcfunction(L, l_draw_frect);
    lua_setglobal(L, "draw_filled_rect");

    lua_pushcfunction(L, l_draw_circle);
    lua_setglobal(L, "draw_circle");

    lua_pushcfunction(L, l_draw_fcircle);
    lua_setglobal(L, "draw_filled_circle");

    lua_pushcfunction(L, l_draw_ellipse);
    lua_setglobal(L, "draw_ellipse");

    lua_pushcfunction(L, l_draw_fellipse);
    lua_setglobal(L, "draw_filled_ellipse");
    

    //force joystick event handling on
    SDL_JoystickEventState(SDL_ENABLE);

    //open up all available joysticks
    std::list<SDL_Joystick*> joystick_list;
    for(int i=0; i < SDL_NumJoysticks(); i++){
        joystick_list.push_back(SDL_JoystickOpen(i));
        std::cout << "Opened Joystick: " << SDL_JoystickName(i) << std::endl;
    }
       
    //nil bits of the os table for safety reasons
    luaL_loadstring(L, "os.execute=nil os.exit=nil os.remove=nil os.rename=nil os.tmpname=nil");
    lua_pcall(L, 0, 0, 0);
       
    //load up the main script into the lua environment
    //and make it all happen
    int error_main = luaL_loadfile(L, "base/init.lua") ||
        lua_pcall(L, 0, 0, 0);
    
    if ( error_main) { // die a horrible death b/c we can't run without that file
        cerr << lua_tostring(L, -1) << "\n";
        lua_pop(L,1);
        
        //close any opened joysticks
        for(std::list<SDL_Joystick*>::iterator it=joystick_list.begin(); it != joystick_list.end(); it++){
            SDL_JoystickClose(*it);
        }

        //end it all
        TTF_Quit();
		sound_close();
        clear_images();
        SDL_Quit();
        lua_close(L);
        return 1;
    }

    //close any opened joysticks
    for(std::list<SDL_Joystick*>::iterator it=joystick_list.begin(); it != joystick_list.end(); it++){
        SDL_JoystickClose(*it);
    }


    //end fonts
    TTF_Quit();

    //close audio
	sound_close();

    //cleanup image store
    clear_images();

    //kill sdl
    SDL_Quit();
    
    //kill lua
    lua_close(L);

    return 0;
}
