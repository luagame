/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/

#include <map>
#include <string>

#include <GL/gl.h>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include "gl_surface.h"

SDL_Surface *screen;
std::map<std::string, GLSurface *> image_store;
Mix_Music * _music;
std::string _music_filename;
std::map<std::string, Mix_Chunk *> sample_cache;
