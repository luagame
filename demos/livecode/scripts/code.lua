--[[
This is a template for livecoding.

It is required() by the main.lua.

One wants to code the global_update(), global_draw(), and global_collide() routines
Put any new variables after the "declare variables here" line
--]]


--Set the FPS here (game speed depends on this)
target_fps = 30

--declare variables here


--set up the events here: evman variable


--updating stuff goes here
function global_update()

end


--drawing stuff goes here
function global_draw()

end


--collision stuff goes here
function global_collide()

end
