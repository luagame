--[[
File: enemies.lua
Author: Brett Lajzer
Date: 5.30.2006

About:
Loads up all enemies.
--]]

all_enemies = {}

require "scripts/enemies/soup_can"
require "scripts/enemies/swooper"