--[[
File: backgrounds.lua
Author: Brett Lajzer
Date: 5.25.2006

About:
This file contains various functions that draw backgrounds.
--]]

backgrounds = {}

--background images
backgrounds.star_bg = get_image("img/star_bg.bmp")


--background variables
backgrounds.offset = 0


--background-drawing functions

function backgrounds.stars_fixed()

  local wide = math.ceil(s_width/128)
  local tall = math.ceil(s_height/128)

  for i = 0, wide do
    for j = 0, tall do
      display(backgrounds.star_bg, 128 * i, 128 *j,0,1,1,255)
    end
  end

end


function backgrounds.stars_scrolling(speed)

  local wide = math.ceil(s_width/128)
  local tall = math.ceil(s_height/128)

  for i = 0, wide do
    for j = -1, tall do
      display(backgrounds.star_bg, 128 * i, 128 *j + backgrounds.offset,0,1,1,255)
    end
  end
  backgrounds.offset = backgrounds.offset + (speed or 4)
  if backgrounds.offset >= 128 then backgrounds.offset = 0 end
end
