--[[
File: soup_can.lua
Author: Brett Lajzer
Date: 5.30.2006

About:
A Warholian enemy.
This enemy showcases parametric movement.
--]]

all_enemies.soup_can = enemy:new()

all_enemies.soup_can.amplitude = 50
all_enemies.soup_can.x_r = 0
all_enemies.soup_can.hp = 2
all_enemies.soup_can.ticks = 0

all_enemies.soup_can.image, all_enemies.soup_can.w, all_enemies.soup_can.h = get_image("img/enemy_soup.png")

all_enemies.soup_can.bullet_image, all_enemies.soup_can.bullet_w, all_enemies.soup_can.bullet_h = get_image("img/soup_drop.png")

all_enemies.soup_can.rects = ObjectList:new()
all_enemies.soup_can.rects:push_back(Rect:new({x=0, y=0, w=all_enemies.soup_can.w, h=all_enemies.soup_can.h}))

function all_enemies.soup_can:update()
  self.step = self.step + 1
  self.ticks = self.ticks + 1
  self.x = self.x_r + self.amplitude * (math.cos((self.ticks*3*math.pi)/180) * self.speed)
  self.y = self.y + self.speed

  --bounds checking (ensure it's inside the game screen)
  if self.x - 100 > s_width or self.y - 100 > s_height or self.x + self.w + 100 < 0 or self.y + self.h + 100 < 0 then
    self.collect = true
  end

  --fire a bullet every 15 steps
  if not self.no_collide and self.step == 15 then
    self:fire()
    self.step = 0
  end
end


function all_enemies.soup_can:fire()
  bullets:push_back(bullet:new(
    {image = self.bullet_image, w = self.bullet_w,
    h = self.bullet_h, x = (self.x + self.w/2 - self.bullet_w/2),
    y = (self.y + self.h), heading = 270, speed = 15}))
end
