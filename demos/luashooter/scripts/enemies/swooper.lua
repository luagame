--[[
File: swooper.lua
Author: Brett Lajzer
Date: 5.31.2006

About:
A swooping enemy.
This enemy showcases parametric movement.
--]]

all_enemies.swooper = enemy:new()

all_enemies.swooper.hp = 1
all_enemies.swooper.ticks = 0
all_enemies.direction = "left"

all_enemies.swooper.image, all_enemies.swooper.w, all_enemies.swooper.h = get_image("img/enemy_swooper.png")

all_enemies.swooper.bullet_image, all_enemies.swooper.bullet_w, all_enemies.swooper.bullet_h = get_image("img/swooper_bullet.png")

all_enemies.swooper.rects = ObjectList:new()
all_enemies.swooper.rects:push_back(Rect:new({x=0, y=0, w=all_enemies.swooper.w, h=all_enemies.swooper.h}))

function all_enemies.swooper:update()
  self.step = self.step + 1
  self.ticks = self.ticks + 1  if self.direction == "left" then self.x = -50 + (1/8 * self.ticks^2) end
  if self.direction == "right" then self.x = 50 + s_width - (1/8 * self.ticks^2) end
  self.y = self.y + self.speed

  --bounds checking (ensure it's inside the game screen)
  if self.x - 100 > s_width or self.y - 100 > s_height or self.x + self.w + 100 < 0 or self.y + self.h + 100 < 0 then
    self.collect = true
  end

  --fire a bullet every 5 steps
  if not self.no_collide and self.step == 5 then
    self:fire()
    self.step = 0
  end
end


function all_enemies.swooper:fire()
  bullets:push_back(bullet:new(
    {image = self.bullet_image, w = self.bullet_w,
    h = self.bullet_h, x = (self.x + self.w/2 - self.bullet_w/2),
    y = (self.y + self.h), heading = 270, speed = 15}))
end
