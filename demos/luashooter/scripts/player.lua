--[[
File: player.lua
Author: Brett Lajzer
Date: 5.24.2006

About:
This file contains the player table.
--]]

--The Player
player = Object:new()
player.lives = 3
player.lives_max = 3
player.shield = 255
player.shield_prev = 0
player.shield_max = 255
player.image, player.w, player.h = get_image("img/player_ship2.png")
player.bullet_image, player.bullet_w, player.bullet_h = get_image("img/diamond_bullet.png")
player.shield_image, player.s_w, player.s_h = get_image("img/shield.png")

--collision rectangles
player.rects = ObjectList:new()


function player.fire()
  player_bullets:push_back(bullet:new({x = (player.x + player.w/2 - bullet.w/2), y = (player.y - 16), heading = 90, speed = 15, image = player.bullet_image, w = player.bullet_w, h = player.bullet_h}))
end

function player.collide()
  player.shield = player.shield - 30
  if player.shield <= 0 then
    player.lives = player.lives - 1
    player.shield = player.shield_max
  end
end

function player.draw()

  if player.shield < player.shield_max then player.shield = player.shield + 1 end
  display(player.shield_image, player.x - player.w/2, player.y - player.h/2, 0, player.shield/player.shield_max, player.shield/player.shield_max,255)  

  --draw player
  display(player.image, player.x, player.y,0,1,1,255)

  player.shield_prev = player.shield
end
