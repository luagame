--[[
File: gameover.lua
Author: Brett Lajzer
Date: 5.31.2006

About:
The screen that shows up when you get a gameover.
--]]

gameover = {}


function gameover.level(init)
  if init == true then
    --clear ObjectLists
    bullets:clear()
    player_bullets:clear()
    enemies:clear()

    draw_bg = backgrounds.stars_scrolling

    --make gameover enemy
    enemies:push_back(gameover.label:new({image = get_image("img/gameover.png"), w = 288, h = 115, x = (s_width - 288)/2, y = (s_height - 115)/2}))
  end
end


-- enemy representing the gameover label
gameover.label = enemy:new()


function gameover.label:update()
  --random movement
  self.x = self.x + math.random(-1, 1)
  self.y = self.y + math.random(-1, 1)
end

function gameover.label:collide()
  --does nothing
end
