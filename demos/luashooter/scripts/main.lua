--[[
File: main.lua
Author: Brett Lajzer
Date: 5.22.2006

About:
This is the main lua script.
It runs everything and makes the magic happen.
--]]


--load up player, enemies, and such
require "scripts/player"
require "scripts/life_bar"
require "scripts/bullet"
require "scripts/backgrounds"
require "scripts/enemy"
require "scripts/scoreboard"
require "scripts/menu"
require "scripts/gameover"
require "scripts/all_enemies"
require "scripts/levels"require "scripts/events"


--global vars
done = false	--used for the main game loop
fire_rate = 0  --used for limiting fire
ticks = 0  --clock ticks (frames since start of level)

bullets = ObjectList:new()  --resource manager for bullets
player_bullets = ObjectList:new() --resource manager for player bullets
enemies = ObjectList:new()  --resource manager for enemies

level = menu.level  --the level function (runs every tick)
new_level = nil  --don't need to do this, it just shows that the var exists
draw_bg = backgrounds.stars_scrolling --the background to draw
e_step = 0

--turn cursor off since player is effectively the cursor
show_cursor(false)
--initialize the event manager and load it with events
evman = EventManager:new()

evman.keyboard.pressed[Keys.ESCAPE] = function() done = true end
evman.quit = function() done = true end

--mouse control scheme
evman.mouse.motion = move_player
evman.mouse.pressed[MouseButtons.BUTTON_LEFT] = function() left = true end
evman.mouse.pressed[MouseButtons.BUTTON_RIGHT] = function() right = true end
evman.mouse.released[MouseButtons.BUTTON_LEFT] = function() left = false end
evman.mouse.released[MouseButtons.BUTTON_RIGHT] = function() right = false end

--joystick control scheme
evman.joystick.axis_motion[1] = move_player_joydir
evman.joystick.pressed[1] = function(button) if (button % 2 + 1) == 2 then button1 = true else button2 = true end end
evman.joystick.released[1] = function(button) if (button % 2 + 1) == 2 then button1 = false else button2 = false end end


--random number generator stuff
do
  local c = 0
  function rrand()
    if c == 600 then
      c = 0
      math.randomseed(os.time())
    end
    c = c + 1
  end
end

--init menu
level(true)

fps = FPSManager:new()
fps:set_fps(30)


while done ~= true do
  rrand()
  --process events
  evman:gather_events()

  --left mouse button fires
  --hold for auto
  if (left or button1) and fire_rate >= 3 then 
    player.fire() --put in limiter
    fire_rate = 0
   end
 
  --right mouse button ends game
  if (right or button2) then
    level = menu.level
    level(true)
    ticks = 0
    scoreboard.score = 0
    player.lives = player.lives_max
  end

  level() -- call the level function

  --update everything
  bullets:update()
  player_bullets:update()
  enemies:update()
  move_player_joy()

  --draw everything
  draw_bg(bg_speed)
  enemies:draw()
  player_bullets:draw()
  player.draw()
  bullets:draw()

  --draw ui stuff
  draw_lives()
  scoreboard.draw()

  --redraw screen
  update_screen()

  --calc collisions
  check_collisions_obj_list(player, bullets)
  check_collisions_lists(enemies, player_bullets)

  --end game if player loses
  if player.lives <= -1 then
    player.lives = player.lives_max
    new_level = gameover.level
  end

  --firing rate limiter  
  fire_rate = fire_rate + 1
  if fire_rate > 3 then fire_rate = 3 end

  --update clock ticks
  ticks = ticks + 1

  if new_level then
    level = new_level
    level(true)
    ticks = 0
    new_level = nil
  end

  --delay to maintain proper fps
  fps:update()

end
