--[[
File: config.lua
Author: Brett Lajzer
Date: 5.23.2006

About:
This is the configuration file. It doesn't need to be
and should not be referenced from any other file. It
is loaded upon program execution and contains variables
that hold configuration values. Because of the way in
which it is loaded, it only needs to be loaded once.
The program does, however use these values in both Lua
and the C++ driver.
--]]

--variables that define the
--screen properties

s_width = 640
s_height = 480
s_depth = 32
s_fullscreen = 0

--the name of the game. Appears in titlebar
s_gamename = "LuaShooter"
