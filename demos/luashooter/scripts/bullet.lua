--[[
File: bullet.lua
Author: Brett Lajzer
Date: 5.24.2006

About:
This is the basic bullet object.
All bullets inherit from it.
--]]


--default values
bullet = Object:new()
bullet.type = "Bullet"

bullet.image, bullet.w, bullet.h = get_image("img/bullet.png")
bullet.rects = ObjectList:new()
bullet.rects:push_back(Rect:new({x=0, y=0, w=bullet.w, h=bullet.h}))

--creates a new bullet
function bullet:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end


--updates a bullet's position
function bullet:update()
  Object.update(self)

  --bounds checking (ensure it's inside the game screen)
  if self.x > s_width or self.y > s_height or self.x + self.w < 0 or self.y + self.h < 0 then
    self.collect = true
  end

end


function bullet:collide()
  self.collect = true
end


--draws a bullet
function bullet:draw()
  Object.draw(self)
end
