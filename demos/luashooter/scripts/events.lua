--[[
File: events.lua
Author: Brett Lajzer
Date: 2.7.2007

About:
This is the main lua script.
It runs everything and makes the magic happen.
--]]

move_dir_x = 0
move_dir_y = 0
move_speed = 10
joy_thresh = 0.15*32767

function move_player(x,y)
    player.x = x - player.w/2
    player.y = y - player.h/2
end

function move_player_joydir(axis, val)
  if axis == 1 then
    if math.abs(val) > joy_thresh then
      if val > 0 then
        move_dir_x = (val^2)/1073676289
      else
        move_dir_x = -1*(val^2)/1073741824
      end
    else move_dir_x = 0 end
  elseif axis == 2 then
    if math.abs(val) > joy_thresh then
      if val > 0 then
        move_dir_y = (val^2)/1073676289
      else
        move_dir_y = -1*(val^2)/1073741824
      end
    else move_dir_y = 0 end
  end
end

function move_player_joy()
  player.x = player.x + math.ceil(move_speed * move_dir_x)
  player.y = player.y + math.ceil(move_speed * move_dir_y)
  if player.x < -player.w/2  then player.x = -player.w/2  end
  if player.x > s_width - player.w/2 then player.x = s_width - player.w/2  end
  if player.y < -player.w/2 then player.y = -player.w/2 end
  if player.y > s_height -player.h/2 then player.y = s_height - player.h/2 end
end
