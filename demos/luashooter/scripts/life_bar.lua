--[[
File: life_bar.lua
Author: Brett Lajzer
Date: 5.24.2006

About:
This function draws the remaining number of lives
horizontally across the bottom left of the screen.
--]]


function draw_lives()
  for i=0,player.lives-1 do
    display(player.image, i*(player.w/2), s_height-player.h,0,0.5,0.5,255)
  end
end