--[[
File: level1.lua
Author: Brett Lajzer
Date: 5.29.2006

About:
This is the first level.
--]]

local counter = 0
local dir = "left"

function levels.level1(init)
  --initialization function
  if init == true then

    --clear res_mans
    bullets:clear()
    player_bullets:clear()
    enemies:clear()

    --set bg
    draw_bg = backgrounds.stars_scrolling
    bg_speed = 4
  end


  --the rest of everything is based on ticks...

  if (ticks >= 10 and ticks % 20 == 10 and ticks <= 2500)  or (ticks >= 3500 and ticks % 30 == 10 and ticks <= 4500) then
    enemies:push_back(all_enemies.soup_can:new({x_r = math.random((s_width - all_enemies.soup_can.w)/6,5*(s_width - all_enemies.soup_can.w)/6), y = -50, speed = 3}))
  end


  if ticks == 2300 then
    bg_speed = 5
  end

  if ticks == 2400 then
    bg_speed = 10
  end

  if ticks == 2500 then
    bg_speed = 20
  end


  if ticks >= 2560 and ticks <= 4000 then

    if counter > 10 and dir == "left" then dir = "right" counter = 0 end
    if counter > 10 and dir == "right" then dir = "left" counter = 0 end

    --generate a line of enemies
    if (ticks % 250) % 7 == 0 then
      enemies:push_back(all_enemies.swooper:new({x = -50, y = -50, speed = 7, direction = dir}))
      counter = counter + 1
    end

  end


end
