--[[
File: menu.lua
Author: Brett Lajzer
Date: 5.29.2006

About:
This is the menu and all related stuff.
--]]

menu = {}


function menu.level(init)
  if init == true then
    --clear resource managers
    bullets:clear()
    player_bullets:clear()
    enemies:clear()

    draw_bg = backgrounds.stars_fixed

    --make menu options
    enemies:push_back(menu.play_enemy:new({image = get_image("img/menu_play.png"), w = 128, h = 128, x = (s_width - 128)/4, y = (s_height - 128)/2}))
    enemies:push_back(menu.quit_enemy:new({image = get_image("img/menu_quit.png"), w = 128, h = 128, x = 3*(s_width - 128)/4, y = (s_height - 128)/2}))
    enemies:push_back(menu.logo:new({image = get_image("img/logo.png"), w = 270, h = 60, x = (s_width - 270)/2, y = 5}))
  end
end


-- enemy representing the play option
menu.play_enemy = enemy:new()


function menu.play_enemy:update()
  --random movement
  self.x = self.x + math.random(-1, 1)
  self.y = self.y + math.random(-1, 1)
end

function menu.play_enemy:collide()
  new_level = levels.level1
end

--enemy representing the quit option
menu.quit_enemy = menu.play_enemy:new()

function menu.quit_enemy:collide()
  done = true
end

--logo
menu.logo = menu.play_enemy:new()

function menu.logo:collide()
  --does nothing
end
