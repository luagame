--[[
File: scoreboard.lua
Author: Brett Lajzer
Date: 5.24.2006

About:
This contains facilities for drawing the scoreboard.
--]]


scoreboard = {}
scoreboard.score = 0
scoreboard.image, scoreboard.w, scoreboard.h = get_image("img/digits.png")
scoreboard.w = scoreboard.w / 10

function scoreboard.draw()
  local temp = "" .. scoreboard.score

  for i=1, #temp do
    local digit = string.sub(temp, i, i)

    if digit == "0" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 0, 0, 1, 1,255)
    elseif digit == "1" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 1, 0, 1, 1,255)
    elseif digit == "2" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 2, 0, 1, 1,255)
    elseif digit == "3" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 3, 0, 1, 1,255)
    elseif digit == "4" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 4, 0, 1, 1,255)
    elseif digit == "5" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 5, 0, 1, 1,255)
    elseif digit == "6" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 6, 0, 1, 1,255)
    elseif digit == "7" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 7, 0, 1, 1,255)
    elseif digit == "8" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 8, 0, 1, 1,255)
    elseif digit == "9" then
      display_frame(scoreboard.image, s_width - (scoreboard.w * (#temp - i + 1)), s_height - scoreboard.h, 10, 9, 0, 1, 1,255)
    end
         
  end

end
