--[[
File: enemy.lua
Author: Brett Lajzer
Date: 5.25.2006

About:
This file contains the basic enemy implementation.
--]]


enemy = Object:new()
enemy.step = 0
enemy.type = "Enemy"
enemy.hp = 5
enemy.points = 5
enemy.image, enemy.w, enemy.h = get_image("img/enemy.bmp")
enemy.explosion, enemy.e_w, enemy.e_h = get_image("img/explosion.png")
enemy.e_w = 64
enemy.e_anim = nil

enemy.rects = ObjectList:new()
enemy.rects:push_back(Rect:new({x=0, y=0, w=enemy.w, h=enemy.h}))


function enemy:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.e_anim = create_animation_iterator(6, 100, 1)
  return o
end


function enemy:update()
  self.step = self.step + 1  Object.update(self)

  --bounds checking (ensure it's inside the game screen)
  if self.x - 100 > s_width or self.y - 100 > s_height or self.x + self.w + 100 < 0 or self.y + self.h + 100 < 0 then
    self.collect = true
  end

  --fire a bullet every 15 steps
  if not self.no_collide and self.step == 15 then
    self:fire()
    self.step = 0
  end
end

function enemy:collide()
  self.hp = self.hp - 1
  if self.hp == 0 then
    self.no_collide = true
    scoreboard.score = scoreboard.score + self.points
  end
end

function enemy:draw()
  Object.draw(self)

  --explosion!
  if self.no_collide then
    local frame, anim = self.e_anim()
    display_frame(self.explosion, self.x + self.w/2 - self.e_w/2, self.y + self.h/2 -self.e_h/2, 6, frame,0,1,1,255)
	if anim == false then self.collect = true end
  end

end


function enemy:fire()
  bullets:push_back(bullet:new({x = (self.x + self.w/2 - bullet.w/2), y = (self.y + self.h), heading = 270, speed = 15}))
end
