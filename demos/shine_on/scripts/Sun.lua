Sun = Object:new()
Sun.type = "Sun"
Sun.image, Sun.w, Sun.h = get_image("images/sun_base.png")
Sun.image_o, Sun.w_o, Sun.h_o = get_image("images/sun_overlay.png")
Sun.image_b = get_image("images/sunbeam.png")

Sun.x_offset = 64
Sun.y_offset = 0
Sun.x = 320
Sun.y = 0
Sun.rd = 0
Sun.sp = 0
Sun.heading = 0
Sun.speed_mov = 8
Sun.beam_time = 0

Sun.beam_on = false

function Sun:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.in_lerp = create_relative_lerp(20,0,255)
  o.out_lerp = create_relative_lerp(20,255,0)
  return o
end

function Sun:draw()
	if self.beam_on == true then
		display(self.image_b,self.x-32,self.y+64,0,1,1, self.b_alpha)
	end
	
	Object.draw(self)
	display(self.image_o, self.x-self.x_offset, self.y-self.y_offset, 0, 1, 1, 255)
end

function Sun:update(delta)
	if self.beam_on then
	  self.beam_time = self.beam_time - 1
	  if self.beam_time > 20 then
	    self.b_alpha = self.in_lerp(1)
	  else
	    self.b_alpha = self.out_lerp(1) 
	  end 
	end
	
	if self.beam_on and self.beam_time <= 0 then self.beam_on = false beam_psys_o.on = false beam_psys_w.on = false end
	self.speed = (math.cos(math.rad(self.sp))*self.speed_mov)

	Object.update(self,delta)
	Sun_psys.x = self.x
	Sun_psys.y = self.y + 16
	self.rotation = math.cos(math.rad(self.rd))*20
	self.rd = self.rd + 1.5
	self.sp = self.sp + 2.0
	if self.rd > 360 then self.rd = self.rd - 360 end
	if self.sp > 360 then self.sp = self.sp - 360 end 
end

function Sun:collide(ids, object)

end