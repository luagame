--default values
Phazon = Object:new()
Phazon.type = "Phazon"

Phazon.image, Phazon.w, Phazon.h = get_image("images/phazon.png")
Phazon.fallspeed = 0Phazon.y_accel = 0.06

function Phazon:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end


function Phazon:update(delta)
  Object.update(self, delta)

  self.fallspeed = self.fallspeed + self.y_accel

  --gravity
  self.y = self.y + self.fallspeed

  --bounds checking (ensure it's inside the game screen)
  if self.x > s_width or self.y > s_height or self.x + self.w < 0 or self.y + self.h < 0 then
    self.collect = true
  end

end


function Phazon:collide(ids, object)
  if object.type == "Rectangle" then
    dark_height = dark_height + 3
  end
  if object.type == "Player" then
    object.charges = object.charges + 1
    score = score + 1
  end
  self.collect = true
end


function Phazon:draw()
  Object.draw(self)
end
