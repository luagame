--default values
Darkle = Object:new()
Darkle.type = "Darkle"

Darkle.image, Darkle.w, Darkle.h = get_image("images/darkle.png")
Darkle.fallspeed = 0Darkle.y_accel = -0.03
Darkle.rect = Rect:new({x=8, y=8, w=16, h=16})

function Darkle:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end


function Darkle:update(delta)
  Object.update(self, delta)
  
  self.y_accel = self.y_accel - 0.001
  
  self.fallspeed = self.fallspeed + self.y_accel

  --gravity
  self.y = self.y + self.fallspeed

  --bounds checking (ensure it's inside the game screen)
  if self.x > s_width or self.y > s_height or self.x + self.w < 0 or self.y + self.h < 0 then
    self.collect = true
  end

end


function Darkle:collide(ids, object)
  self.collect = true
end


function Darkle:draw()
  Object.draw(self)
end
