--default values
TinyPhazon = Object:new()
TinyPhazon.type = "TinyPhazon"

TinyPhazon.image, TinyPhazon.w, TinyPhazon.h = get_image("images/tiny_phazon.png")

function TinyPhazon:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.lerp = create_relative_lerp(5,255,0)
  return o
end


function TinyPhazon:update(delta)
  Object.update(self, delta)

  self.alpha = self.lerp(1)

  --bounds checking (ensure it's inside the game screen)
  if self.x > s_width or self.y > s_height or self.x + self.w < 0 or self.y + self.h < 0 then
    self.collect = true
  end

end


function Phazon:draw()
  Object.draw(self)
end
