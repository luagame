require "scripts/TinyPhazon"

--default values
Player = Object:new()
Player.type = "Player"

Player.image_l, Player.w, Player.h = get_image("images/player_left.png")
Player.image_r = get_image("images/player_right.png")
Player.image_c = get_image("images/player_charged.png")
Player.left = false
Player.right = false
Player.heading = 0
Player.charges = 0

Player.psys = ParticleSystem:new()
Player.psys.object = TinyPhazon
Player.psys.angle_min = 45
Player.psys.angle_max = 135
Player.psys.speed_min = 4
Player.psys.speed_max = 7
Player.psys.particles_per_update_min = 5
Player.psys.particles_per_update_max = 10
Player.psys.life_min = 1
Player.psys.life_max = 5
Player.psys.on = false

function Player:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.lerp = create_absolute_lerp(20,20,8)
  return o
end


function Player:update(delta)
  if self.right == true and self.left == true then
	self.speed = 0
  elseif self.right == true then
	self.speed = 5
  elseif self.left == true then
	self.speed = -5
  else
  	self.speed = 0
  end
  
  Object.update(self, delta)
  
  if self.x < 0 then self.x = 0 end
  if self.x > 640 - 32 then self.x = 640 - 32 end

  if self.charges < 20 then
    self.psys.on = false
  else
    self.psys.on = true
  end
  
  self.psys.x = self.x + 8
  self.psys.y = self.y
  self.psys:update()

end


function Player:shoot()
	if self.charges >= 20 then
		bullet_list:push_back(PhazonBlast:new({x=self.x, y=self.y, heading=90, speed=10}))
		self.charges = self.charges - 20
	end
end


function Player:draw()
  self.psys:draw()
  if self.charges < 20 then
    local dist = self.lerp(self.charges) 
  
    display(self.image_l, self.x-self.x_offset-dist, self.y-self.y_offset, self.rotation, 1, 1, 255)
    display(self.image_r, self.x-self.x_offset+dist, self.y-self.y_offset, self.rotation, 1, 1, 255)
  else
    display(self.image_c, self.x-self.x_offset, self.y-self.y_offset, self.rotation, 1, 1, 255)
  end
end
