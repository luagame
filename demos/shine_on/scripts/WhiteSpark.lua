--default values
WhiteSpark = Object:new()
WhiteSpark.type = "WhiteSpark"

WhiteSpark.image, WhiteSpark.w, WhiteSpark.h = get_image("images/white_sparkle.png")

function WhiteSpark:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end


function WhiteSpark:update(delta)
  Object.update(self, delta)

  --bounds checking (ensure it's inside the game screen)
  if self.x > s_width or self.y > s_height or self.x + self.w < 0 or self.y + self.h < 0 then
    self.collect = true
  end

end


function WhiteSpark:draw()
  Object.draw(self)
end
