--[[
This script is the main script. It requires the code.lua file,
which is where one really wants to do work.
--]]

--global vars
done = false	--used for the main game loop
ticks = 0  --clock ticks (frames since start of level)

--turn cursor off
show_cursor(false)
instr_log("cursor off", "input")

--initialize the event manager
-- set up ESC key exit default
evman = EventManager:new()
evman.keyboard.pressed[Keys.ESCAPE] = function() done = true end
evman.quit = function() done = true end
instr_log("created evman", "input", "gameplay")

--require the code
require("scripts/code")
instr_log("included real code", "gameplay")

--fps stuff 
fps = FPSManager:new()
fps:set_fps(target_fps)
instr_log("set fps","gameplay","video")

--random number generator stuff
-- reseeds the random number generator periodically
do
  local c = 0
  function rrand()
    if c == target_fps*100 then
      c = 0
      math.randomseed(os.time())
    end
    c = c + 1
  end
end

rrand()

titlescreen = get_image("images/title.png")
instr_log("+title screen", "event")
title = true
instr_log("random classless event")
--title screen loop
while done ~= true do
  evman:gather_events()
  display(titlescreen,0,0,0,1,1,255)
  
  if title == false then
    done = true
  end
    --redraw screen
  update_screen()

  --update clock ticks
  ticks = ticks + 1

  --delay to maintain proper fps
  fps:update()
end
instr_log("-title screen", "event")
done = false
instr_log("+main loop", "event", "gameplay")
--main game loop
while done ~= true do
  rrand()

  --process events
  evman:gather_events()

  --calc collisions
  global_collide()

  --update everything
  global_update()

  --draw everything
  global_draw()

  --redraw screen
  update_screen()

  --update clock ticks
  ticks = ticks + 1

  --delay to maintain proper fps
  fps:update()

end
instr_log("-main loop", "event", "gameplay")
instr_stats()
instr_print_log("event")
instr_print_log("gameplay")
instr_print_log("input")
instr_print_log("input","gameplay")
instr_print_log()
