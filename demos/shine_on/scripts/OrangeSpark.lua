--default values
OrangeSpark = Object:new()
OrangeSpark.type = "OrangeSpark"

OrangeSpark.image, OrangeSpark.w, OrangeSpark.h = get_image("images/orange_sparkle.png")

function OrangeSpark:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end


function OrangeSpark:update(delta)
  Object.update(self, delta)
  --bounds checking (ensure it's inside the game screen)
  if self.x > s_width or self.y > s_height or self.x + self.w < 0 or self.y + self.h < 0 then
    self.collect = true
  end

end


function OrangeSpark:draw()
  Object.draw(self)
end
