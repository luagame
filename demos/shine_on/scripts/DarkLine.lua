--default values
DarkLine = Object:new()
DarkLine.type = "DarkLine"

DarkLine.image, DarkLine.w, DarkLine.h = get_image("images/darkline.png")
DarkLine.fallspeed = 0DarkLine.y_accel = -0.03
DarkLine.lerp = nil -- will hold the transparency lerp

function DarkLine:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  o.anim_func = create_animation_iterator(8, 200, 1)
  o.lerp = create_relative_lerp(60, 255, 0)
  return o
end


function DarkLine:update(delta)
  Object.update(self, delta)
  
  self.y_accel = self.y_accel - 0.002
  
  self.fallspeed = self.fallspeed + self.y_accel

  --gravity
  self.y = self.y + self.fallspeed

  --bounds checking (ensure it's inside the game screen)
  if self.x > s_width or self.y > s_height or self.x + self.w < 0 or self.y + self.h < 0 or self.alpha == 0 then
    self.collect = true
  end

  self.alpha = self.lerp(1)

end


function DarkLine:collide(ids, object)
  self.collect = true
end


function DarkLine:draw()
  local frame, playing = self.anim_func()
  if playing == false then frame = 7 end
  display_frame(self.image, self.x-self.x_offset, self.y-self.y_offset, 8, frame,0,1,1, self.alpha)
end
