--default values
Sparkle = Object:new()
Sparkle.type = "Sparkle"

Sparkle.image, Sparkle.w, Sparkle.h = get_image("images/sparkle.png")
Sparkle.fallspeed = 0Sparkle.y_accel = 0.035
Sparkle.rect = Rect:new({x=8, y=8, w=16, h=16})

function Sparkle:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end


function Sparkle:update(delta)
  Object.update(self, delta)

  self.fallspeed = self.fallspeed + self.y_accel

  --gravity
  self.y = self.y + self.fallspeed

  --bounds checking (ensure it's inside the game screen)
  if self.x > s_width or self.y > s_height or self.x + self.w < 0 or self.y + self.h < 0 then
    self.collect = true
  end

end


function Sparkle:collide(ids, object)
  if object.type == "Darkle" then
    phazon_list:push_back(Phazon:new({x=self.x,y=self.y}))
  end
  if object.type == "Rectangle" then
    dark_height = dark_height - 1.5
    if dark_height < 0 then dark_height = 0 end
  end
  self.collect = true
end


function Sparkle:draw()
  Object.draw(self)
end
