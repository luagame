--[[
This is a template for livecoding.

It is required() by the main.lua.

One wants to code the global_update(), global_draw(), and global_collide() routines
Put any new variables after the "declare variables here" line
--]]

--use the axis-aligned collision code (significantly faster)
require "base/aabb_collision"
require "scripts/Sparkle"
require "scripts/Sun"

require "scripts/Darkle"
require "scripts/DarkLine"
require "scripts/Phazon"
require "scripts/PhazonBlast"
require "scripts/Player"
require "scripts/OrangeSpark"
require "scripts/WhiteSpark"

bg_image = get_image("images/bg.png")
dark_overlay = get_image("images/dark_overlay.png")
thesun = Sun:new()

--Set the FPS here (game speed depends on this)
target_fps = 45

--declare variables here
gameover = false

score = 0
score_label = Label:new()
score_label:set_font("VeraBd.ttf", 20)
score_label:set_caption(score.."")
score_label:update()
score_label.x = 0
score_label.y = 480 - score_label.h

gameover_label = Label:new()
gameover_label:set_font("VeraBd.ttf", 48)
gameover_label:set_caption("GAME OVER")
gameover_label:update()
gameover_label.x = 320
gameover_label.y = 240
gameover_label.x_offset = gameover_label.w/2
gameover_label.y_offset = gameover_label.h/2

player = Player:new()
player.x = 320-32

darktangle = Rectangle:new({x=0,y=0,w=640,h=0,style="filled"})
dark_height = 0

phazon_list = ObjectList:new()
bullet_list = ObjectList:new()

Sun_psys = ParticleSystem:new()
Sun_psys.object = Sparkle
Sun_psys.angle_min = 180
Sun_psys.angle_max = 360
Sun_psys.speed_min = 0.75
Sun_psys.speed_max = 2.0
Sun_psys.life_min = 100
Sun_psys.life_max = 200
Sun_psys.update_delay = 8
Sun_psys.on = true

Dark_psys = ParticleSystem:new()
Dark_psys.object = Darkle
Dark_psys.type ="rect"
Dark_psys.rect = Rect:new({x=0,y=480-32,w=640,h=32})
Dark_psys.angle_min = 80
Dark_psys.angle_max = 100
Dark_psys.speed_min = 0.1
Dark_psys.speed_max = 0.35
Dark_psys.life_min = 100
Dark_psys.life_max = 200
Dark_psys.update_delay = 3
Dark_psys.on = true

Dark_psys2 = ParticleSystem:new()
Dark_psys2.object = DarkLine
Dark_psys2.type ="rect"
Dark_psys2.rect = Rect:new({x=0,y=480-32,w=640,h=32})
Dark_psys2.angle_min = 90
Dark_psys2.angle_max = 90
Dark_psys2.speed_min = 0.2
Dark_psys2.speed_max = 0.6
Dark_psys2.particles_per_update_min = 1
Dark_psys2.particles_per_update_max = 8
Dark_psys2.life_min = 50
Dark_psys2.life_max = 200
Dark_psys2.on = true


beam_psys_o = ParticleSystem:new()
beam_psys_o.object = OrangeSpark
beam_psys_o.type ="rect"
beam_psys_o.rect = Rect:new({x=0,y=0,w=64,h=32})
beam_psys_o.angle_min = 0
beam_psys_o.angle_max = 180
beam_psys_o.speed_min = 4
beam_psys_o.speed_max = 7.0
beam_psys_o.life_min = 2
beam_psys_o.life_max = 8
beam_psys_o.particles_per_update_min = 5
beam_psys_o.particles_per_update_max = 25
beam_psys_o.on = false

beam_psys_w = ParticleSystem:new()
beam_psys_w.object = WhiteSpark
beam_psys_w.type ="rect"
beam_psys_w.rect = Rect:new({x=0,y=0,w=64,h=32})
beam_psys_w.angle_min = 180
beam_psys_w.angle_max = 360
beam_psys_w.speed_min = 4
beam_psys_w.speed_max = 7.0
beam_psys_w.life_min = 2
beam_psys_w.life_max = 8
beam_psys_w.particles_per_update_min = 5
beam_psys_w.particles_per_update_max = 25
beam_psys_w.on = false


--set up the events here: evman variable
evman.keyboard.pressed[Keys.LEFT] = function() player.left = true end
evman.keyboard.released[Keys.LEFT] = function() player.left = false end

evman.keyboard.pressed[Keys.RIGHT] = function() player.right = true end
evman.keyboard.released[Keys.RIGHT] = function() player.right = false end

evman.keyboard.pressed[Keys.SPACE] = function() player:shoot() end
evman.keyboard.pressed[Keys.RETURN] = function() title = false end

--updating stuff goes here
function global_update()
	if gameover ~= true then
		player.y = 430 - dark_height
		player:update()
		phazon_list:update()
		bullet_list:update()
		thesun:update()
		instr_begin("update_psys")
		Sun_psys:update()
		Dark_psys:update()
		Dark_psys2:update()
		beam_psys_o.rect.x = thesun.x - 40
		beam_psys_w.rect.x = thesun.x - 40
		beam_psys_o.rect.y = 432-dark_height
		beam_psys_w.rect.y = 448-dark_height
		beam_psys_o:update()
		beam_psys_w:update()
		instr_track("psys", Dark_psys2.object_list.size)
		instr_end("update_psys")
		dark_height = dark_height + 0.05
		darktangle.y = 480 - dark_height
		darktangle.h = dark_height
		Dark_psys.rect.y = 448 - dark_height
		Dark_psys2.rect.y = 448 - dark_height
		score_label:set_caption(score.."")
		score_label:update()
		if dark_height >= 480-128-32 then gameover = true darktangle.a = 5 end
		if thesun.beam_on == true then
			dark_height = dark_height - 0.4
			score = score + 1
		end
	else
		darktangle.h = 640
		darktangle.y = 0
		score_label.x = 320
		score_label.x_offset = score_label.w/2
		score_label.y = 240 + 64
	end
end


--drawing stuff goes here
function global_draw()
	if gameover ~= true then
		--draw background
		display(bg_image,0,0,0,1,1,255)
		instr_begin("draw_psys")
		Sun_psys:draw()
		Dark_psys:draw()
		Dark_psys2:draw()
		instr_end("draw_psys")
		phazon_list:draw()
		thesun:draw()
		darktangle:draw()
		bullet_list:draw()
		display(dark_overlay,0,448-dark_height,0,1,1,255)
		beam_psys_o:draw()
		beam_psys_w:draw()
		player:draw()
		score_label:draw()
	else
		darktangle:draw()
		gameover_label:draw()
		score_label:draw()
	end
end


--collision stuff goes here
function global_collide()
	if gameover ~= true then
		instr_begin("collisions")
		check_collisions_lists(Sun_psys.object_list, Dark_psys.object_list)
		check_collisions_obj_list(darktangle, phazon_list)
		check_collisions_obj_list(player, phazon_list)
		check_collisions_obj_list(thesun, bullet_list)
		check_collisions_obj_list(darktangle, Sun_psys.object_list)
		instr_end("collisions")
	end	
end
