--default values
PhazonBlast = Object:new()
PhazonBlast.type = "PhazonBlast"

PhazonBlast.image, PhazonBlast.w, PhazonBlast.h = get_image("images/phazon_blast.png")
PhazonBlast.x_offset = PhazonBlast.w/2

function PhazonBlast:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end


function PhazonBlast:update(delta)
  Object.update(self, delta)
  --bounds checking (ensure it's inside the game screen)
  if self.x > s_width or self.y > s_height or self.x + self.w < 0 or self.y + self.h < 0 then
    self.collect = true
    score = score - 20
  end

end


function PhazonBlast:collide(ids, object)
  if object.type == "Sun" then
    object.beam_on = true
    object.beam_time = object.beam_time + 120
    beam_psys_o.on = true
    beam_psys_w.on = true
    object.in_lerp(0,true)
    object.out_lerp(0,true)
  end
  self.collect = true
end


function PhazonBlast:draw()
  Object.draw(self)
end
