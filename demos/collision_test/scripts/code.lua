--[[
This is a template for livecoding.

It is required() by the main.lua.

One wants to code the global_update(), global_draw(), and global_collide() routines
Put any new variables after the "declare variables here" line
--]]


--Set the FPS here (game speed depends on this)
target_fps = 30

--declare variables here
red = Object:new()
red.image, red.h, red.w = get_image("images/red_rectangle.png")
red.collide = function(self)
  print(os.time().." collision!")
end

blue = Object:new()
blue.image, blue.h, blue.w = get_image("images/blue_rectangle.png")
blue.x = 100
blue.y = 100
blue.angular_velocity = 1

--set up the events here: evman variable
evman.mouse.motion = function(x,y) red.x = x red.y = y test.x = x test.y = y end
evman.mouse.pressed[1] = function() red.angular_velocity = 1 end
evman.mouse.released[1] = function() red.angular_velocity = 0 end

--updating stuff goes here
function global_update()
  red:update()
  blue:update()
end


--drawing stuff goes here
function global_draw()
  fill_screen(0,0,0)
  blue:draw()
  red:draw()
end


--collision stuff goes here
function global_collide()
  check_collisions_obj_obj(red, blue)
end
