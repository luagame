--[[
This script is the main script. It requires the code.lua file,
which is where one really wants to do work.
--]]

--global vars
done = false	--used for the main game loop
ticks = 0  --clock ticks (frames since start of level)

--turn cursor off
show_cursor(false)

--initialize the event manager
-- set up ESC key exit default
evman = EventManager:new()
evman.keyboard.pressed[Keys.ESCAPE] = function() done = true end
evman.quit = function() done = true end

--require the code
require("scripts/code")

--fps stuff 
fps = FPSManager:new()
fps:set_fps(target_fps)

--random number generator stuff
-- reseeds the random number generator periodically
do
  local c = 0
  function rrand()
    if c == target_fps*100 then
      c = 0
      math.randomseed(os.time())
    end
    c = c + 1
  end
end

--main game loop
while done ~= true do
  rrand()

  --process events
  evman:gather_events()

  --calc collisions
  global_collide()

  --update everything
  global_update()

  --draw everything
  global_draw()

  --redraw screen
  update_screen()

  --update clock ticks
  ticks = ticks + 1

  --delay to maintain proper fps
  fps:update()

end
