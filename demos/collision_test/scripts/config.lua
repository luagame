--variables that define the
--screen properties

s_width = 640
s_height = 480
s_depth = 32
s_fullscreen = 0

--the name of the game. Appears in titlebar
s_gamename = "collision test"
