MyObj = Object:new()

MyObj.type = "MyObj"
MyObj.rects = ObjectList:new()
MyObj.image, MyObj.w, MyObj.h = get_image("data/images/wikilogo.png")

function MyObj:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

function MyObj:reflect_x()
  self.heading = (-1 * self.heading) + 180
  self.heading = self.heading % 360
end

function MyObj:reflect_y()
  self.heading = -1 * self.heading
  self.heading = self.heading % 360
end

function MyObj:update(delta)
  Object.update(self, delta)
    
  --this code is what makes it bounce
  local cx, cy = Object.get_center(self)
  
  if cx <= 0 then
    self.x = 0 - math.floor(self.w/2)
    self:reflect_x()
  end
  
  if cx >= s_width then
    self.x = s_width - math.floor(self.w/2)
    self:reflect_x()
  end
  
  if cy <= 0 then
    self.y = 0 - math.floor(self.h/2)
    self:reflect_y()
  end
  
  if cy >= s_height then
    self.y = s_height - math.floor(self.h/2)
    self:reflect_y()
  end
end

--silly
function MyObj:collide(ids, object)
  print("Ouch! You hit me, "..object.type.." at "..os.time().."!")
end