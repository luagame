require("scripts/MyObj")

evman = EventManager:new()
evman.quit = function () done = true end
evman.keyboard.pressed[Keys.ESCAPE] = function() done = true end

--controls main game loop
done = false

fps = FPSManager:new() -- create the FPSManager object
fps:set_fps(30) -- set the FPS to 30

--create a MyObj
mo = MyObj:new()
math.randomseed(os.time())
mo.heading = math.random(15,80) -- prevent shallow angles
mo.speed = 3
mo.angular_velocity = 5

--create another MyObj
mo1 = MyObj:new()
mo1.heading = math.random(15,80) -- prevent shallow angles
mo1.speed = 3
mo1.angular_velocity = 5

--create yet another MyObj
mo2 = MyObj:new()
mo2.heading = math.random(15,80) -- prevent shallow angles
mo2.speed = 3
mo2.angular_velocity = 5

--create an ObjectList
olist = ObjectList:new()
olist:push_back(mo)
olist:push_back(mo1)
olist:push_back(mo2)

--main game loop
while done ~= true do
  evman:gather_events()
  
  check_collisions_lists(olist, olist)
  olist:update()
  
  fill_screen(0,0,0)
  
  olist:draw()
  
  update_screen()
  fps:update() -- delay to ensure proper FPS
end
