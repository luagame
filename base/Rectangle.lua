--the Rectangle class

Rectangle = Object:new()

Rectangle.type = "Rectangle"
Rectangle.r = 0
Rectangle.g = 0
Rectangle.b = 0
Rectangle.a = 255
Rectangle.style = "outline" --can be "outline", "filled", or "both"
Rectangle.fcolor = false --set to true to have the filled rect use a different color
Rectangle.fr = 0
Rectangle.fg = 0
Rectangle.fb = 0
Rectangle.fa = 255


function Rectangle:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

--updates the object's state
function Rectangle:update(delta)
  Object.update(self, delta)
end

--default draw routine
function Rectangle:draw()
  if(self.style == "filled" or self.style == "both") then
    if(self.fcolor == true) then
      draw_filled_rect(self.x, self.y, self.x+self.w, self.y+self.h, self.fr, self.fg, self.fb, self.fa, self.rotation, self.scale_x, self.scale_y)
    else
      draw_filled_rect(self.x, self.y, self.x+self.w, self.y+self.h, self.r, self.g, self.b, self.a, self.rotation, self.scale_x, self.scale_y)
    end
  end

  if(self.style == "outline" or self.style == "both") then
    draw_rect(self.x, self.y, self.x+self.w, self.y+self.h, self.r, self.g, self.b, self.a, self.rotation, self.scale_x, self.scale_y)
  end
end

--there is no predefined collision behavior
-- "ids" is the list of Rect ids that have been collided with
-- "type" is the type of the Object that is colliding
function Rectangle:collide(type, ids)

end

