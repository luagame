--Label class
--A text label
--
-- set_font() must be called before the label can be displayed

Label = Object:new()
Label.type = "Label"
Label.size=12 --the default font size
Label.font=nil --the pointer to the font
Label.font_file=nil --the file to load
Label.caption = nil --the text of the label
Label.changed = false --a flag to determine if the label has changed

--the color of the Label
Label.red = 255
Label.green = 255
Label.blue = 255

function Label:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

--changes the font size
function Label:change_size(s)
  if size ~= s then
    self.size = s
    if font ~= nil then
      unload_font(font)
    end
    self.font = load_font(self.font_file, self.size)
    self.changed = true
  end
end

function Label:set_font(f, s)
    if font ~= nil then
      unload_font(font)
    end
    self.font = load_font(f, s)
    self.font_file = f
    self.size = s
    self.changed = true
end

function Label:set_caption(s)
  if s ~= self.caption then
    self.caption = s
    self.changed = true
  end
end

--updates the object's state
function Label:update()
  if self.image ~= nil and self.changed == true then
    delete_image(self.image)
  end

  if self.changed == true then
    self.image, self.w, self.h = render_string(self.font, self.caption, self.red, self.green, self.blue)
    self.changed = false
  end

  Object.update(self)
end

--default draw routine
function Label:draw()
  if self.collect == false then
    Object.draw(self)
  end
end
