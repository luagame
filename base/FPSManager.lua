--[[
  FPSManager Class
  ================
  
  Used for doing FPS management.
  Really simple, but convenient. Use it.

--]]

FPSManager = {}
FPSManager.delta = 0 --access this

--don't touch any of these. use the methods instead
FPSManager.fps = 0
FPSManager.time_per_frame = 0
FPSManager.previous_frame = 0

--constructor
function FPSManager:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

--sets the FPS and time_per_frame
function FPSManager:set_fps(fps)
  self.fps = fps
  self.time_per_frame = math.floor(1000 / fps)
  self.previous_frame = get_ticks()
end

--updates the fps manager and returns the delta
function FPSManager:update()
  self.delta = get_ticks() - self.previous_frame
  self.previous_frame = get_ticks()

  --delay and recalculate the delta
  if self.delta < self.time_per_frame then
    delay(self.time_per_frame - self.delta)
    self.delta = get_ticks() - self.previous_frame
  end
end
