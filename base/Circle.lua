--the Circle class

Circle = Object:new()

Circle.type = "Circle"
Circle.radius = 0
Circle.r = 0
Circle.g = 0
Circle.b = 0
Circle.a = 255
Circle.style = "outline" --can be "outline", "filled", or "both"
Circle.fcolor = false --set to true to have the filled circle use a different color
Circle.fr = 0
Circle.fg = 0
Circle.fb = 0
Circle.fa = 255


function Circle:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

--updates the object's state
function Circle:update(delta)
  Object.update(self, delta)
end

--default draw routine
function Circle:draw()
  if(self.style == "filled" or self.style == "both") then
    if(self.fcolor == true) then
      draw_filled_circle(self.x, self.y, self.radius, self.fr, self.fg, self.fb, self.fa, self.rotation, self.scale_x, self.scale_y)
    else
      draw_filled_circle(self.x, self.y, self.radius, self.r, self.g, self.b, self.a, self.rotation, self.scale_x, self.scale_y)
    end
  end

  if(self.style == "outline" or self.style == "both") then
    draw_circle(self.x, self.y, self.radius, self.r, self.g, self.b, self.a, self.rotation, self.scale_x, self.scale_y)
  end
end

--there is no predefined collision behavior
-- "ids" is the list of Rect ids that have been collided with
-- "type" is the type of the Object that is colliding
function Circle:collide(type, ids)

end

