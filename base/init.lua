--Lua Game base script library initialization

require "base/ObjectList"
require "base/Rect"
require "base/Object"
require "base/Line"
require "base/Rectangle"
require "base/Circle"
require "base/Ellipse"
require "base/Label"
require "base/ParticleSystem"
require "base/obb_collision"
require "base/key_syms"
require "base/mouse_defs"
require "base/EventManager"
require "base/FPSManager"
require "base/animation"

--this isn't required. enable if you need to do profiling.
require "base/instrumentation"

--Load up the main lua script
require "scripts/main"
