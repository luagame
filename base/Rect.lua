--Rect object (used for collision detection)

Rect = {}
Rect.x = 0
Rect.y = 0
Rect.w = 0
Rect.h = 0
Rect.id = "" --string returned from collision tests


--creates a new rect
function Rect:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end
