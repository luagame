--the Ellipse class

Ellipse = Object:new()

Ellipse.type = "Ellipse"
Ellipse.radius_x = 0
Ellipse.radius_y = 0
Ellipse.r = 0
Ellipse.g = 0
Ellipse.b = 0
Ellipse.a = 255
Ellipse.style = "outline" --can be "outline", "filled", or "both"
Ellipse.fcolor = false --set to true to have the filled circle use a different color
Ellipse.fr = 0
Ellipse.fg = 0
Ellipse.fb = 0
Ellipse.fa = 255


function Ellipse:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

--updates the object's state
function Ellipse:update(delta)
  Object.update(self, delta)
end

--default draw routine
function Ellipse:draw()
  if(self.style == "filled" or self.style == "both") then
    if(self.fcolor == true) then
      draw_filled_ellipse(self.x, self.y, self.radius_x, self.radius_y, self.fr, self.fg, self.fb, self.fa, self.rotation, self.scale_x, self.scale_y)
    else
      draw_filled_ellipse(self.x, self.y, self.radius_x, self.radius_y, self.r, self.g, self.b, self.a, self.rotation, self.scale_x, self.scale_y)
    end
  end

  if(self.style == "outline" or self.style == "both") then
    draw_ellipse(self.x, self.y, self.radius_x, self.radius_y, self.r, self.g, self.b, self.a, self.rotation, self.scale_x, self.scale_y)
  end
end

--there is no predefined collision behavior
-- "ids" is the list of Rect ids that have been collided with
-- "type" is the type of the Object that is colliding
function Ellipse:collide(type, ids)

end

