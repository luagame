--the Line class

Line = Object:new()

Line.type = "Line"
Line.x1 = 0
Line.y1 = 0
Line.x2 = 0
Line.y2 = 0
Line.r = 0
Line.g = 0
Line.b = 0
Line.a = 255

function Line:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

--updates the object's state
function Line:update(delta)
  Object.update(self, delta)
end

--default draw routine
function Line:draw()
  draw_line(self.x+self.x1,self.y+self.y1,self.x+self.x2,self.y+self.y2,self.r,self.g,self.b,self.a)
end

--there is no predefined collision behavior
-- "ids" is the list of Rect ids that have been collided with
-- "type" is the type of the Object that is colliding
function Line:collide(type, ids)

end

