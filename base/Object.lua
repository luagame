--the Object class
--all objects should descend from this

Object = {}
Object.x = 0
Object.y = 0
Object.w = 0
Object.h = 0

Object.x_offset = 0
Object.y_offset = 0

--movement
Object.heading = 0  --heading in degrees
Object.speed = 0  --speed in pixels

--rotation
Object.rotation = 0
Object.angular_velocity = 0

--scaling (applied before rotation)
Object.scale_x = 1
Object.scale_y = 1

--Alpha value, applies to an entire image
Object.alpha = 255

Object.image = nil --this should hold the image of the Object
Object.rects = ObjectList:new() --this field should hold the collision Rect objects

Object.type = "Object" --for each class, this should be set to a unique identifier. it is used in collision detection

Object.max_updates = -1 --if set to a non-negative, non-zero number, object will automatically collect itself after this many updates
Object.num_updates = 0 --current count of updates

Object.no_collide = false --setting this to true will prevent it from colliding
Object.collect = false  --set to true to cause resource manager to collect this object 

function Object:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  return o
end

--updates the object's state
function Object:update(delta)
  if delta ~= nil then
    delta = delta/1000
  end
  
  self.x = self.x + math.cos(math.rad(self.heading)) * (self.speed * (delta or 1))
  self.y = self.y + math.sin(math.rad(-1 * self.heading)) * (self.speed * (delta or 1))
  self.rotation = (self.rotation + (self.angular_velocity * (delta or 1))) % 360
  
  --Object self-collect check (mostly for particle systems)
  if self.max_updates > 0 then
    self.num_updates = self.num_updates + 1
    if self.num_updates >= self.max_updates then
      self.collect = true
    end
  end

end

--default draw routine
function Object:draw()
  display(self.image, self.x-self.x_offset, self.y-self.y_offset, self.rotation, self.scale_x, self.scale_y, self.alpha)
end

--there is no predefined collision behavior
-- "ids" is the list of Rect ids that have been collided with
-- "object" is the Object that is colliding with this one
function Object:collide(ids, object)

end

--returns the coordinate of the center
function Object:get_center()
  return math.floor(self.x + (self.w/2)), math.floor(self.y + (self.h/2))
end

--sets the drawing offset of the sprite
function Object:set_origin(x,y)
  self.x_offset = x
  self.y_offset = y
end

--unloads resources and nils out the class' table
--  There is no defined behavior for this function in the Object class. 
function Object:unload()
	--normally, resources would be unloaded here
	--and one would put a line such as
	--ClassName = nil at the end
end