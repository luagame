#Installation prefix
PREFIX = /usr

#compilation variables
CXX = g++
LDFLAGS := ${LDFLAGS} -llua -lSDL -lSDL_image -lSDL_mixer -lSDL_gfx -lSDL_ttf
CFLAGS := ${CFLAGS} -O2 -I/usr/include

objects = funcs_draw.o funcs_font.o funcs.o funcs_input.o funcs_sound.o funcs_video.o globals.o
doc_source = doc/class_eventmanager.txt doc/index.txt doc/class_objectlist.txt doc/class_object.txt doc/fun_ref.txt

all: luagame

luagame: main.o $(objects)
	${CXX} -o luagame main.o $(objects) ${LDFLAGS}

main.o : $(objects) main.cpp globals.h
	${CXX} -c main.cpp ${CFLAGS}
	
funcs_draw.o : funcs_draw.cpp funcs_draw.h globals.h
	${CXX} -c funcs_draw.cpp ${CFLAGS}

funcs_font.o : funcs_font.cpp funcs_font.h globals.h
	${CXX} -c funcs_font.cpp ${CFLAGS}

funcs.o : funcs.cpp funcs.h globals.h
	${CXX} -c funcs.cpp ${CFLAGS}

funcs_input.o : funcs_input.cpp funcs_input.h globals.h
	${CXX} -c funcs_input.cpp ${CFLAGS}

funcs_sound.o : funcs_sound.cpp funcs_sound.h globals.h
	${CXX} -c funcs_sound.cpp ${CFLAGS}
	
funcs_video.o : funcs_video.cpp funcs_video.h globals.h
	${CXX} -c funcs_video.cpp $(CFLAGS)

globals.o : globals.h globals.cpp
	${CXX} -c globals.cpp $(CFLAGS)

doc: $(doc_source)
	cd doc
	./build_docs.sh
	cd ..

clean:
	rm -f main.o $(objects)
	rm -f luagame

install: luagame
	strip luagame
	cp luagame ${PREFIX}/bin/
	
install-doc: doc
	mkdir ${PREFIX}/share/luagame/
	mkdir ${PREFIX}/share/luagame/doc
	cd doc
	cp *.html ${PREFIX}/share/luagame/doc/
	cd ..

uninstall:
	rm -f ${PREFIX}/bin/luagame
	rm -rf ${PREFIX}/share/luagame

