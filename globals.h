/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/

#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <map>
#include <string>

#include <GL/gl.h>
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>
#include "gl_surface.h"

extern SDL_Surface *screen;
extern std::map<std::string, GLSurface *> image_store;
extern Mix_Music * _music;
extern std::string _music_filename;
extern std::map<std::string, Mix_Chunk *> sample_cache;
#endif
