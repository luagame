/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/

#include <cmath>
#include <GL/gl.h>
#include <GL/glu.h>
#include <lua.hpp>
#include <SDL/SDL.h>
#include "globals.h"
#include "funcs_draw.h"

//
// direct drawing functions
//

//internal function for drawing circles and ellipses
void drawCircle(GLenum style, float x, float y, float rx, float ry, float r, float g, float b, float a, float rot, float scale_x, float scale_y){
    glColor4f(r, g, b, a);
	glDisable(GL_TEXTURE_RECTANGLE_ARB);
	glPushMatrix();
	glTranslatef(x, y, 0.0f);
	glScalef(scale_x, scale_y, 1.0f);
	glRotatef(rot, 0.0f, 0.0f, -1.0f);
	
	glBegin(style);
	  for(double t = 0; t < 2*3.14159265358979; t += 0.05) {
	    glVertex2f(rx * cos(t), ry * sin(t));
	  }	  
	glEnd();
	
	glPopMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_RECTANGLE_ARB);	
}

int l_draw_pixel(lua_State *L){	
	glColor4f(lua_tonumber(L,3)/255.0, lua_tonumber(L,4)/255.0, lua_tonumber(L,5)/255.0, lua_tonumber(L,6)/255.0);
	glDisable(GL_TEXTURE_RECTANGLE_ARB);
	
	glBegin(GL_POINTS);
	  glVertex2f((float)lua_tonumber(L,1), (float)lua_tonumber(L,2));
	glEnd();
	
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_RECTANGLE_ARB);
	return 0;
}

int l_draw_line(lua_State *L){
	float x1 = (float)lua_tonumber(L,1);
	float y1 = (float)lua_tonumber(L,2);
	float x2 = (float)lua_tonumber(L,3);
	float y2 = (float)lua_tonumber(L,4);
	  
	glColor4f(lua_tonumber(L,5)/255.0, lua_tonumber(L,6)/255.0, lua_tonumber(L,7)/255.0, lua_tonumber(L,8)/255.0);
	glDisable(GL_TEXTURE_RECTANGLE_ARB);
	
	glBegin(GL_LINES);
	  glVertex2f(x1,y1);
	  glVertex2f(x2,y2);
	glEnd();
	
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_RECTANGLE_ARB);
	return 0;
}

int l_draw_rect(lua_State *L){
	float x1 = (float)lua_tonumber(L,1);
	float y1 = (float)lua_tonumber(L,2);
	float x2 = (float)lua_tonumber(L,3);
	float y2 = (float)lua_tonumber(L,4);
	float rot = (float)lua_tonumber(L,9);
	float scale_x = (float)lua_tonumber(L,10);
	float scale_y = (float)lua_tonumber(L,11);
	  
	glColor4f(lua_tonumber(L,5)/255.0, lua_tonumber(L,6)/255.0, lua_tonumber(L,7)/255.0, lua_tonumber(L,8)/255.0);
	glDisable(GL_TEXTURE_RECTANGLE_ARB);
	
	float w_half = (x2-x1)/2;
	float h_half = (y2-y1)/2;
	
	glPushMatrix();
	glTranslatef(x1+w_half, y1+h_half, 0.0f);
	
	glScalef(scale_x, scale_y, 1.0f);
	glRotatef(rot, 0.0f, 0.0f, -1.0f);
	
	glBegin(GL_LINE_LOOP);
	  glVertex2f(-w_half, -h_half);
	  glVertex2f(-w_half, h_half);
	  glVertex2f(w_half, h_half);
	  glVertex2f(w_half, -h_half);	
	glEnd();
	glPopMatrix();
	
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_RECTANGLE_ARB);
	return 0;
}

int l_draw_frect(lua_State *L){
	float x1 = (float)lua_tonumber(L,1);
	float y1 = (float)lua_tonumber(L,2);
	float x2 = (float)lua_tonumber(L,3);
	float y2 = (float)lua_tonumber(L,4);
	float rot = (float)lua_tonumber(L,9);
	float scale_x = (float)lua_tonumber(L,10);
	float scale_y = (float)lua_tonumber(L,11);

    glColor4f(lua_tonumber(L,5)/255.0, lua_tonumber(L,6)/255.0, lua_tonumber(L,7)/255.0, lua_tonumber(L,8)/255.0);
    glDisable(GL_TEXTURE_RECTANGLE_ARB);
    
    float w_half = (x2-x1)/2;
	float h_half = (y2-y1)/2;
	
	glPushMatrix();
	glTranslatef(x1+w_half, y1+h_half, 0.0f);
	
	glScalef(scale_x, scale_y, 1.0f);
	glRotatef(rot, 0.0f, 0.0f, -1.0f);
    
	glBegin(GL_QUADS);
	  glVertex2f(-w_half, -h_half);
	  glVertex2f(-w_half, h_half);
	  glVertex2f(w_half, h_half);
	  glVertex2f(w_half, -h_half);	
	glEnd();
	glPopMatrix();
	
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_TEXTURE_RECTANGLE_ARB);
	return 0;
}

int l_draw_circle(lua_State *L){	
    drawCircle(GL_LINE_LOOP, (float)lua_tonumber(L,1), (float)lua_tonumber(L,2), (float)lua_tonumber(L,3), (float)lua_tonumber(L,3), lua_tonumber(L,4)/255.0, lua_tonumber(L,5)/255.0, lua_tonumber(L,6)/255.0, lua_tonumber(L,7)/255.0, (float)lua_tonumber(L,8), (float)lua_tonumber(L,9), (float)lua_tonumber(L,10));
	return 0;
}

int l_draw_fcircle(lua_State *L){
	drawCircle(GL_POLYGON, (float)lua_tonumber(L,1), (float)lua_tonumber(L,2), (float)lua_tonumber(L,3), (float)lua_tonumber(L,3), lua_tonumber(L,4)/255.0, lua_tonumber(L,5)/255.0, lua_tonumber(L,6)/255.0, lua_tonumber(L,7)/255.0, (float)lua_tonumber(L,8), (float)lua_tonumber(L,9)/255.0, (float)lua_tonumber(L,10));
	return 0;
}

int l_draw_ellipse(lua_State *L){
	drawCircle(GL_LINE_LOOP, (float)lua_tonumber(L,1), (float)lua_tonumber(L,2), (float)lua_tonumber(L,3), (float)lua_tonumber(L,4), lua_tonumber(L,5)/255.0, lua_tonumber(L,6)/255.0, lua_tonumber(L,7)/255.0, lua_tonumber(L,8)/255.0, (float)lua_tonumber(L,9),(float)lua_tonumber(L,10), (float)lua_tonumber(L,11));
	return 0;
}

int l_draw_fellipse(lua_State *L){
	drawCircle(GL_POLYGON, (float)lua_tonumber(L,1), (float)lua_tonumber(L,2), (float)lua_tonumber(L,3), (float)lua_tonumber(L,4), lua_tonumber(L,5)/255.0, lua_tonumber(L,6)/255.0, lua_tonumber(L,7)/255.0, lua_tonumber(L,8)/255.0, (float)lua_tonumber(L,9), (float)lua_tonumber(L,10), (float)lua_tonumber(L,11));
	return 0;
}
