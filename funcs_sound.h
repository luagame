/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/

#ifndef _FUNCS_SOUND_H_
#define _FUNCS_SOUND_H_

#include "lua.hpp"
//open sound device
void sound_open();

//close sound device
void sound_close();

//play a sample
int l_playsample(lua_State *L);

//stop all samples
int l_stopsamples(lua_State *L);

//preload a sample
int l_loadsample(lua_State *L);

//unload a sample
int l_unloadsample(lua_State *L);

//clear the sample cache
int l_clearsamples(lua_State *L);

//play music
int l_playmusic(lua_State *L);

//stop music
int l_stopmusic(lua_State *L);

#endif
