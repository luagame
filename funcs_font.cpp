/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/

#include <map>
#include <string>
#include <lua.hpp>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include "funcs_font.h"
#include "funcs_video.h"
#include "gl_surface.h"

//loads a font and returns a pointer to it
int l_load_font(lua_State *L){
  TTF_Font *font;
  font = TTF_OpenFont(luaL_checkstring(L,1), (int)lua_tonumber(L,2));

  lua_pushlightuserdata(L,font);
  return 1;
}

//unloads a font
int l_close_font(lua_State *L){
  TTF_CloseFont((TTF_Font*)lua_touserdata(L,1));
  lua_pushnil(L);
  return 1;
}

//gets dimensions of a string as rendered with a font
int l_font_string_metrics(lua_State *L){
  int w, h;
  TTF_SizeUTF8((TTF_Font*)lua_touserdata(L,1), luaL_checkstring(L,2), &w, &h);

  lua_pushinteger(L, w);
  lua_pushinteger(L, h);

  return 2;
}

//render a string in a font
int l_render_string(lua_State *L){
  SDL_Surface *temp;
  SDL_Color color;

  color.r = (int)lua_tonumber(L,3);
  color.g = (int)lua_tonumber(L,4);
  color.b = (int)lua_tonumber(L,5);

  temp = TTF_RenderUTF8_Blended((TTF_Font*)lua_touserdata(L,1),luaL_checkstring(L,2), color);

  GLuint new_tex = surfaceToTexture(L, temp);
  
  GLSurface *final = new GLSurface(new_tex, temp->w, temp->h);
  
  SDL_FreeSurface(temp);
  
  lua_pushlightuserdata(L, final);
  lua_pushinteger(L, final->w);
  lua_pushinteger(L, final->h);

  return 3;
}
