/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/
#ifndef _FUNCS_INPUT_H_
#define _FUNCS_INPUT_H_
#include "lua.hpp"

//returns the number of joysticks
int l_num_joysticks(lua_State *L);

//get mouse position
int l_mouse_state(lua_State *L);

//return an event to lua
int l_get_event(lua_State *L);

#endif
