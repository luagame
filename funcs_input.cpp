/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/

#include "SDL/SDL.h"#include "lua.hpp"
#include "globals.h"
#include "funcs_input.h"

//returns the number of joysticks
int l_num_joysticks(lua_State *L){
  lua_pushinteger(L, SDL_NumJoysticks());
  return 1;
}

//get mouse position
int l_mouse_state(lua_State *L){
  int x=0; int y=0; bool left=false; bool right=false;
  
  SDL_PumpEvents();
  Uint8 mouse_state = SDL_GetMouseState(&x, &y);
  if(mouse_state&SDL_BUTTON(1))
    left = true;
  if(mouse_state&SDL_BUTTON(3))
    right = true;
  
  lua_pushinteger(L, (lua_Integer)x);
  lua_pushinteger(L, (lua_Integer)y);
  lua_pushboolean(L, left);
  lua_pushboolean(L, right);
  
  return 4;       
}

//return an event to lua
int l_get_event(lua_State *L){
  SDL_Event e;
  
  //if there is an event, then return it, else nil
  if(SDL_PollEvent(&e)){
    switch(e.type){
      case SDL_KEYDOWN:
        lua_pushstring(L, "keyboard");
        lua_pushstring(L, "pressed");
        lua_pushinteger(L, (int)e.key.keysym.sym);
        return 3;
        break;
      case SDL_KEYUP:
        lua_pushstring(L, "keyboard");
        lua_pushstring(L, "released");
        lua_pushinteger(L, (int)e.key.keysym.sym);
        return 3;
        break;
      case SDL_MOUSEMOTION:
        lua_pushstring(L, "mouse");
        lua_pushstring(L, "motion");
        lua_pushinteger(L,(int)e.motion.x);
        lua_pushinteger(L,(int)e.motion.y);
        lua_pushinteger(L,(int)e.motion.xrel);
        lua_pushinteger(L,(int)e.motion.yrel);
        return 6;
        break;
      case SDL_MOUSEBUTTONDOWN:
        lua_pushstring(L, "mouse");
        lua_pushstring(L, "pressed");
        lua_pushinteger(L,(int)e.button.button);
        lua_pushinteger(L,(int)e.button.x);
        lua_pushinteger(L,(int)e.button.y);
        return 5;
        break;
      case SDL_MOUSEBUTTONUP:
        lua_pushstring(L, "mouse");
        lua_pushstring(L, "released");
        lua_pushinteger(L,(int)e.button.button);
        lua_pushinteger(L,(int)e.button.x);
        lua_pushinteger(L,(int)e.button.y);
        return 5;
        break;
      case SDL_JOYAXISMOTION:
        lua_pushstring(L, "joystick");
        lua_pushstring(L, "axis_motion");
        lua_pushinteger(L,(int)(e.jaxis.which+1));
        lua_pushinteger(L,(int)(e.jaxis.axis+1));
        lua_pushinteger(L,(int)e.jaxis.value);
        return 5;
        break;
      case SDL_JOYBALLMOTION:
        lua_pushstring(L, "joystick");
        lua_pushstring(L, "ball_motion");
        lua_pushinteger(L,(int)(e.jball.which+1));
        lua_pushinteger(L,(int)(e.jball.ball+1));
        lua_pushinteger(L,(int)e.jball.xrel);
        lua_pushinteger(L,(int)e.jball.yrel);
        return 6;
        break;
      case SDL_JOYHATMOTION:
        lua_pushstring(L, "joystick");
        lua_pushstring(L, "hat_motion");
        lua_pushinteger(L,(int)(e.jhat.which+1));
        lua_pushinteger(L,(int)(e.jhat.hat+1));
        lua_pushinteger(L,(int)e.jhat.value);
        return 5;
        break;
      case SDL_JOYBUTTONDOWN:
        lua_pushstring(L, "joystick");
        lua_pushstring(L, "pressed");
        lua_pushinteger(L,(int)(e.jbutton.which+1));
        lua_pushinteger(L,(int)(e.jbutton.button+1));
        return 4;
        break;
      case SDL_JOYBUTTONUP:
        lua_pushstring(L, "joystick");
        lua_pushstring(L, "released");
        lua_pushinteger(L,(int)(e.jbutton.which+1));
        lua_pushinteger(L,(int)(e.jbutton.button+1));
        return 4;
        break;
      case SDL_QUIT:
      	lua_pushstring(L, "quit");
      	return 1;
      	break;
      case SDL_SYSWMEVENT:
      case SDL_VIDEORESIZE:
      	break; //not supported yet
      case SDL_VIDEOEXPOSE:
      	break; //not supported yet
      case SDL_USEREVENT:
        lua_pushnil(L);
        return 1;
        break;
    }
  }
  lua_pushnil(L);
  return 1;
}
