#!/bin/bash

for i in $( ls *.txt); do
	echo building: $i
	asciidoc -a icons -a numbered -a toc $i
done
