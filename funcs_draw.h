/*
Copyright (c)2006-2008 - Brett Lajzer

See LICENSE for license information.
*/

#ifndef _FUNCS_DRAW_H_
#define _FUNCS_DRAW_H_

#include <lua.hpp>

//direct drawing functions
int l_draw_pixel(lua_State *L);

int l_draw_line(lua_State *L);

int l_draw_rect(lua_State *L);

int l_draw_frect(lua_State *L);

int l_draw_circle(lua_State *L);

int l_draw_fcircle(lua_State *L);

int l_draw_ellipse(lua_State *L);

int l_draw_fellipse(lua_State *L);

#endif
