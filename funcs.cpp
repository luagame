/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
#include <lua.hpp>
#include <iostream>
#include "funcs.h"

using namespace std;

//prints usage info
void print_usage(string progname){
    cerr << "USAGE: " << progname << " [dir] | [-h|--help]\n";
    cerr << "    dir - directory where game data is\n";
    cerr << "    -h --help - displays this usage info\n\n";
}

//configuration loader
//loads config into lua, retrieves values and leaves them there
SDL_Surface *load_config(lua_State *L, string pname){
    if(luaL_loadfile(L, "scripts/config.lua") || lua_pcall(L,0,0,0)){
      cerr << "Cannot load configuration script!\n" << "Error: " <<lua_tostring(L, -1) << "\n";
      print_usage(pname);
      return 0;
    }

    //put the config values on the stack
    lua_getglobal(L, "s_fullscreen");
    lua_getglobal(L, "s_width");
    lua_getglobal(L, "s_height");
    lua_getglobal(L, "s_depth");
    lua_getglobal(L, "s_gamename");
    
    //check values for proper type
    if(!lua_isnumber(L,-5)){
      cerr << "Config Error: "<< "'s_fullscreen' should be a number\n";
      return 0;
    }
    if(!lua_isnumber(L,-4)){
      cerr << "Config Error: "<< "'s_width' should be a number\n";
      return 0;
    }
    if(!lua_isnumber(L,-3)){
      cerr << "Config Error: "<< "'s_height' should be a number\n";
      return 0;
    }
    if(!lua_isnumber(L,-2)){
      cerr << "Config Error: "<< "'s_depth' should be a number\n";
      return 0;
    }
    if(!lua_isstring(L,-1)){
      cerr << "Config Error: "<< "'s_gamename' should be a string\n";
      return 0;
    }
    
    //set caption and return the surface
    SDL_WM_SetCaption(lua_tostring(L,-1), lua_tostring(L, -1));
    
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    
    SDL_Surface *temp = SDL_SetVideoMode(lua_tointeger(L, -4), lua_tointeger(L, -3), lua_tointeger(L, -2), SDL_OPENGL | (lua_tointeger(L,-5)? SDL_FULLSCREEN : 0));
    
    //initialize OpenGL
    glShadeModel(GL_FLAT);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_TEXTURE_RECTANGLE_ARB);
    glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	
	//setup projection
	glViewport(0, 0, (GLsizei)lua_tointeger(L, -4), (GLsizei)lua_tointeger(L, -3));

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

	glOrtho(0.0f, (GLfloat)lua_tointeger(L, -4), (GLfloat)lua_tointeger(L, -3), 0.0f, -1.0f, 1.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
	glClear(GL_COLOR_BUFFER_BIT);  
	
	//cleanup
	for(int i=0; i < 5; i++){lua_pop(L,-1);}
	
    return temp;
}

//function for clearing out the image store
void clear_images(){
    map<string, GLSurface *>::iterator im = image_store.begin();

    for(; im != image_store.end(); im++){
        glDeleteTextures(1, &(im->second->texture));
        delete im->second;
    }

    image_store.clear();
}
