/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/

#include <GL/gl.h>
#include <SDL/SDL.h>
#include <lua.hpp>

//loads an SDL_Surface into a texture and returns the handle
GLuint surfaceToTexture(lua_State *L, SDL_Surface *s);

//function for loading a surface from many different formats (except TIFF)
int l_getimage(lua_State *L);

//forces an image to be unloaded from memory
int l_releaseimage(lua_State *L);

//function for updating the display surface
int l_flip(lua_State *L); 

//function for simple drawing (non-animated)
int l_display(lua_State *L);

//function for blitting a frame of an animation
//frames are laid out horizontally
//frames start at 0
int l_displayframe(lua_State *L);

//cursor toggler
int l_show_cursor(lua_State *L);

//fills screen with a color
int l_fill_screen(lua_State *L);

//free surface interface 
int l_delete_image(lua_State *L);
