/*
Copyright (c)2006-2007 - Brett Lajzer

See LICENSE for license information.
*/
#ifndef _FUNCS_FONT_H_
#define _FUNCS_FONT_H_

#include <map>
#include <string>
#include <lua.hpp>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>

//loads a font and returns a pointer to it
int l_load_font(lua_State *L);

//unloads a font
int l_close_font(lua_State *L);

//gets dimensions of a string as rendered with a font
int l_font_string_metrics(lua_State *L);

//render a string in a font
int l_render_string(lua_State *L);

#endif
