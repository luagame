/*
Copyright (c)2006-2008 - Brett Lajzer

See LICENSE for license information.
*/

#include <iostream>

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <lua.hpp>
#include "globals.h"


//loads an SDL_Surface into a texture and returns the handle
GLuint surfaceToTexture(lua_State *L, SDL_Surface *s){
	GLenum texture_format;
	GLuint tex;
	
	if (s->format->BytesPerPixel == 4){
		if (s->format->Rmask == 0x000000ff)
			texture_format = GL_RGBA;
		else
			texture_format = GL_BGRA;
	} else if (s->format->BytesPerPixel == 3){
		if (s->format->Rmask == 0x000000ff)
			texture_format = GL_RGB;
		else
			texture_format = GL_BGR;
	}else{
		return luaL_error(L, "ERROR: Can't create texture from image. Image must be 24 or 32 bpp.");
	}
	
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tex);
	glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, s->format->BytesPerPixel, s->w, s->h, 0, texture_format, GL_UNSIGNED_BYTE, s->pixels );
	
	return tex;
}


//function for loading a surface from many different formats
int l_getimage(lua_State *L){
  std::string filename(luaL_checkstring(L,1));
  std::map<std::string, GLSurface *>::iterator im = image_store.find(filename);

  //search for the image in the store, if not found, load it
  if(im == image_store.end()){
    SDL_Surface *temp = IMG_Load(filename.c_str());
    
    if(temp == NULL){
    	return luaL_error(L, "ERROR: Can't find image file: \"%s\".", filename.c_str());
    }
    
    GLuint texture = surfaceToTexture(L, temp);
    
    GLSurface *surf = new GLSurface(texture, temp->w, temp->h);
    image_store[filename] = surf;
    lua_pushlightuserdata(L, surf);
    lua_pushinteger(L, (lua_Integer)temp->w);
    lua_pushinteger(L, (lua_Integer)temp->h);
    
    SDL_FreeSurface(temp);
  }else{
    lua_pushlightuserdata(L, im->second);
    lua_pushinteger(L, (lua_Integer)im->second->w);
    lua_pushinteger(L, (lua_Integer)im->second->h);
  }
  return 3;
}

//forces an image to be unloaded from memory
int l_releaseimage(lua_State *L){
  std::string filename(luaL_checkstring(L,1));
  std::map<std::string, GLSurface *>::iterator im = image_store.find(filename);

  if(im != image_store.end()){
	glDeleteTextures(1, &(im->second->texture));
	delete im->second;
    image_store.erase(im);
  }

  return 0;
}


//function for updating the display surface
int l_flip(lua_State *L){
  SDL_GL_SwapBuffers();
  return 0;
}


//function for simple displaying of images (non-animated)
int l_display(lua_State *L){
  GLSurface *tex = (GLSurface *)lua_touserdata(L,1);
  
  float x = (float)lua_tonumber(L,2);
  float y = (float)lua_tonumber(L,3);
  double rot = lua_tonumber(L,4);
  double scale_x = lua_tonumber(L,5);
  double scale_y = lua_tonumber(L,6);
  float alpha = lua_tonumber(L, 7)/255.0;
  
  if(tex == NULL){
  	return luaL_error(L, "ERROR: Can't draw. Image is null.");
  }
  
  float w_half = tex->w/2.0;
  float h_half = tex->h/2.0;
  
  glColor4f(1.0,1.0,1.0, alpha);
  
  glPushMatrix();
  glTranslatef(x + w_half, y + h_half, 0.0f);
  glScalef(scale_x, scale_y, 1.0f);
  glRotatef(rot, 0.0f, 0.0f, -1.0f);
  
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tex->texture);
  //draw here
  glBegin(GL_QUADS);
    // Top-left vertex (corner)
    glTexCoord2i(0, 0);
    glVertex2f(-w_half, -h_half);
    
    // Bottom-left vertex (corner)
    glTexCoord2i(0, tex->h);
    glVertex2f(-w_half, h_half);
    
    // Bottom-right vertex (corner)
    glTexCoord2i(tex->w, tex->h);
    glVertex2f(w_half, h_half);
    
    // Top-right vertex (corner)
    glTexCoord2i(tex->w, 0);
    glVertex2f(w_half, -h_half);
  glEnd();
  
  glPopMatrix();

  return 0;      
}


//function for displaying a frame of an animation
//frames are laid out horizontally
//frames start at 0
int l_displayframe(lua_State *L){
  GLSurface *tex = (GLSurface*)lua_touserdata(L,1);
  float x = (float)lua_tonumber(L,2);
  float y = (float)lua_tonumber(L,3);
  int numframes = lua_tointeger(L,4);
  int frame = lua_tointeger(L,5);
  double rot = lua_tonumber(L,6);
  double scale_x = lua_tonumber(L,7);
  double scale_y = lua_tonumber(L,8);
  float alpha = lua_tonumber(L, 9)/255.0;
  
  if(tex == NULL){
	return luaL_error(L, "ERROR: Can't draw. Image is null.");
  }
  
  int width = tex->w/numframes;
  int frame_offset = frame * width;
  
  float w_half = width/2.0;
  float h_half = tex->h/2.0;
  
  glColor4f(1.0,1.0,1.0, alpha);
   
  glPushMatrix(); 
  glTranslatef(x + w_half, y + h_half, 0.0f);
  glScalef(scale_x, scale_y, 1.0f);
  glRotatef(rot, 0.0f, 0.0f, -1.0f);    
  
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tex->texture);
  //draw here
  glBegin(GL_QUADS);
    // Top-left vertex (corner)
    glTexCoord2i(frame_offset, 0);
    glVertex2f(-w_half, -h_half);
    
    // Bottom-left vertex (corner)
    glTexCoord2i(frame_offset, tex->h);
    glVertex2f(-w_half, h_half);
    
    // Bottom-right vertex (corner)
    glTexCoord2i(frame_offset + width, tex->h);
    glVertex2f(w_half, h_half);
    
    // Top-right vertex (corner)
    glTexCoord2i(frame_offset + width, 0);
    glVertex2f(w_half, -h_half);
  glEnd();
  
  glPopMatrix();
  
  return 0;      
}


//cursor toggler
int l_show_cursor(lua_State *L){
  bool show = lua_toboolean(L, 1);
  
  SDL_ShowCursor((show ? SDL_ENABLE : SDL_DISABLE));
       
  return 0;       
}


//fills screen with a color
int l_fill_screen(lua_State *L){
  glClearColor(lua_tointeger(L,1)/255.0, lua_tointeger(L,2)/255.0, lua_tointeger(L,3)/255.0, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT);  
  return 0;      
}


//free surface interface
int l_delete_image(lua_State *L){
  GLSurface *tmp = (GLSurface*)lua_touserdata(L,1);

  if(tmp == NULL){
  	return luaL_error(L, "ERROR: Can't free. Image is null.");
  }

  glDeleteTextures(1, &(tmp->texture));
  delete tmp;
  lua_pushnil(L);
  return 1;
}

